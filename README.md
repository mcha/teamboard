**** FOR EDUCATIONAL PURPOSE ONLY ****

### MongoDB
  Either register an account on [MongoLabs](https://mlab.com)
   
  Otherwise use a local MongoDB database
  
    mkdir db
    mongod --dbpath db --port 27018
  
    
### Using the application

  1. Edit src/main/resources/application.properties

  2. Configure user/password, host/port, database name

  3. Start it (use --init options **only to setup a demo data set**)  
      
    mvn spring-boot:run -Dspring-boot.run.arguments=--init  
            
  4. See Swagger API Doc : http://localhost:9080/swagger-ui.html 
