package org.cham.tp.teamboard.task;

public enum TaskStatus {

    TODO,
    WIP,
    DONE,
    BLOCKED;

}
