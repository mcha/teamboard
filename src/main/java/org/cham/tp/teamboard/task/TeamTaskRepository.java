package org.cham.tp.teamboard.task;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface TeamTaskRepository extends MongoRepository<TeamTask, String>{
    List<TeamTask> findAllByTeamId(String teamId);
}
