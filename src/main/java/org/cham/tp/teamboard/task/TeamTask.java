package org.cham.tp.teamboard.task;

import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class TeamTask {

    @ApiModelProperty(notes = "id of the task")
    @Id
    String id;
    @ApiModelProperty(notes = "id of the team to which the task belong", required = true)
    String teamId;
    @ApiModelProperty(notes = "label of the task", required = true)
    String label;
    @ApiModelProperty(notes = "Status of the task", allowableValues = "TODO, WIP, DONE, BLOCKED", required = true)
    TaskStatus status;
    @ApiModelProperty(notes = "description of the task")
    String description;

    public TeamTask() {
    }

    public TeamTask(String teamId, String label, TaskStatus status) {
        this.teamId = teamId;
        this.label = label;
        this.status = status;
    }

    public TeamTask(String teamId, String label, TaskStatus status, String description) {
        this(teamId, label, status);
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public TaskStatus getStatus() {
        return status;
    }

    public void setStatus(TaskStatus status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
