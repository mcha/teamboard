package org.cham.tp.teamboard.task;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Example;
import io.swagger.annotations.ExampleProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@Api(value="CRUD API for team tasks", description = "CRUD API for team tasks")
@CrossOrigin
public class TeamTaskController {

    private TeamTaskRepository teamTaskRepository;

    @Autowired
    public TeamTaskController(TeamTaskRepository teamTaskRepository) {
        this.teamTaskRepository = teamTaskRepository;
    }

    @ApiOperation("Find all task of given team if teamId is provided or all tasks of all teams")
    @RequestMapping(value={"teams/{teamId}/tasks", "tasks"}, method = RequestMethod.GET)
    public List<TeamTask> findAll(@PathVariable Optional<String> teamId) {
        return teamId
                .map((id) -> teamTaskRepository.findAllByTeamId(id))
                .orElse(teamTaskRepository.findAll());
    }

    @ApiOperation("Find a task by its id")
    @GetMapping("tasks/{id}")
    public TeamTask findById(@PathVariable String id) {
        return teamTaskRepository.findById(id).get();
    }

    @ApiOperation("Save a new task for a given team")
    @ApiResponses(value= {@ApiResponse(code = 200, message = "id of the created task",
                            examples = @Example({@ExampleProperty("5bc50243e2afd713a8f63f18")}))
    })
    @RequestMapping(value={"teams/{teamId}/tasks", "tasks"}, method = RequestMethod.POST)
    public String create(@PathVariable Optional<String> teamId, @RequestBody TeamTask task) {
        return save(teamId, task);
    }

    @ApiOperation("Update a task for a given team")
    @ApiParam(value = "teamId", required = true)
    @ApiResponses(value= {@ApiResponse(code = 200, message = "id of the updated task",
            examples = @Example({@ExampleProperty("5bc50243e2afd713a8f63f18")}))
    })
    @RequestMapping(value={"teams/{teamId}/tasks", "tasks"}, method = RequestMethod.PUT)
    public String update(@PathVariable Optional<String> teamId, @RequestBody TeamTask task) {
        if (null==task.getId()) throw new IllegalArgumentException("id is mandatory");
        return save(teamId, task);
    }

    private String save(Optional<String> teamId, TeamTask task) {
        teamId.ifPresent((id) -> task.setTeamId(id));
        if (null==task.getTeamId()) throw new IllegalArgumentException("teamId is mandatory");
        return teamTaskRepository.save(task).getId();
    }

    @ApiOperation("Delete a task by its id")
    @DeleteMapping("tasks/{id}")
    public void deleteById(@PathVariable String id) {
        teamTaskRepository.deleteById(id);
    }

}
