package org.cham.tp.teamboard;

import org.cham.tp.teamboard.task.TaskStatus;
import org.cham.tp.teamboard.task.TeamTask;
import org.cham.tp.teamboard.task.TeamTaskRepository;
import org.cham.tp.teamboard.team.Team;
import org.cham.tp.teamboard.team.TeamRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import java.util.stream.Stream;

@EnableMongoRepositories(basePackageClasses = {TeamRepository.class, TeamTaskRepository.class})
@Configuration
public class DBConfig {

    Logger logger = LoggerFactory.getLogger(DBConfig.class);

    @Bean
    ApplicationRunner applicationRunner(TeamRepository teamRepository, TeamTaskRepository teamTaskRepository) {
        return (ApplicationArguments args) -> {
            if (args.getOptionNames().contains("init")) {
                initTeamRocket(teamRepository, teamTaskRepository);
                initBlueTeam(teamRepository, teamTaskRepository);
                logger.info("======= Sample data initialized =======");
            }else{
                logger.info("======= Use --init option to initialize sample data =======");
            }
        };
    }

    private void initBlueTeam(TeamRepository teamRepository, TeamTaskRepository teamTaskRepository) {
        String blueTeamId = teamRepository.save(new Team("Blue Team")).getId();
        Stream.of(new TeamTask(blueTeamId, "Call Alpha Tango Echo Charlie", TaskStatus.DONE),
                new TeamTask(blueTeamId, "US-09 analysis", TaskStatus.DONE),
                new TeamTask(blueTeamId, "US-09 Coding", TaskStatus.WIP),
                new TeamTask(blueTeamId, "US-10 analysis", TaskStatus.TODO),
                new TeamTask(blueTeamId, "US-12 Coding", TaskStatus.TODO))
                .forEach(task -> teamTaskRepository.save(task));
    }

    private void initTeamRocket(TeamRepository teamRepository, TeamTaskRepository teamTaskRepository) {
        String teamRocketId = teamRepository.save(new Team("Team Rocket")).getId();
        Stream.of(new TeamTask(teamRocketId, "Setup Environment", TaskStatus.DONE),
                new TeamTask(teamRocketId, "US-01 analysis", TaskStatus.DONE),
                new TeamTask(teamRocketId, "US-01 Coding", TaskStatus.WIP),
                new TeamTask(teamRocketId, "US-02 analysis", TaskStatus.TODO),
                new TeamTask(teamRocketId, "US-02 Coding", TaskStatus.TODO))
                .forEach(task -> teamTaskRepository.save(task));
    }
}
