package org.cham.tp.teamboard.team;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Example;
import io.swagger.annotations.ExampleProperty;
import org.cham.tp.teamboard.task.TaskStatus;
import org.cham.tp.teamboard.task.TeamTask;
import org.cham.tp.teamboard.task.TeamTaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

@RestController
@RequestMapping("/api/teams")
@Api(value="CRUD API for teams", description = "CRUD API for teams")
@CrossOrigin
public class TeamController {

    private TeamTaskRepository teamTaskRepository;

    private TeamRepository teamRepository;

    @Autowired
    public TeamController(TeamRepository teamRepository, TeamTaskRepository teamTaskRepository) {
        this.teamRepository = teamRepository;
        this.teamTaskRepository = teamTaskRepository;
    }

    @ApiOperation("Find all teams")
    @GetMapping
    public List<Team> findAll() {
        return teamRepository.findAll();
    }

    @ApiOperation("Find a team by its id")
    @GetMapping("{id}")
    public Team findById(@PathVariable String id) {
        return teamRepository.findById(id).get();
    }

    @ApiOperation("Save new team")
    @ApiResponses(value= { @ApiResponse(code = 200, message = "id of the created team",
                           examples = @Example({@ExampleProperty("5bc50243e2afd713a8f63f18")}))
    })
    @PostMapping
    public String create(@RequestBody Team team) {
        return teamRepository.save(team).getId();
    }

    @ApiOperation(value = "Update existing team")
    @PutMapping
    public String update(@RequestBody Team team) {
        if (null==team.getId()) throw new IllegalArgumentException("id is mandatory");
        return teamRepository.save(team).getId();
    }

    @ApiOperation("Delete a team by its id")
    @DeleteMapping("{id}")
    public void deleteById(@PathVariable String id) {
        teamRepository.deleteById(id);
    }

    @ApiOperation("Find stats of all task status TODO/WIP/DONE for all teams")
    @RequestMapping(value={"stats"}, method = RequestMethod.GET)
    public Map<String, Map<TaskStatus, Long>> computeStats() {
        return this.teamTaskRepository
                .findAll()
                .stream()
                .collect(groupingBy(TeamTask::getTeamId, groupingBy(TeamTask::getStatus, counting())));
    }
}
