package org.cham.tp.teamboard.team;

import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Team {

    @ApiModelProperty(notes = "id of the team")
    @Id
    private String id;

    @ApiModelProperty(notes = "name of the team", required = true)
    private String name;

    public Team() {
    }

    public Team(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
